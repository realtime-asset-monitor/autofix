// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package autofix

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, pubsubMsg pubsub.Message) error {
	b, _ := json.MarshalIndent(pubsubMsg, "", "  ")

	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv

	msgTimestamp, err := time.Parse(time.RFC3339, pubsubMsg.Attributes["timestamp"])
	if err != nil {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'timestamp' is missing or invalid and should not", "", "")
		return nil
	}

	now := time.Now()
	d := now.Sub(msgTimestamp)

	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	if pubsubMsg.Attributes["origin"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'origin' is zero value and should not", "", "")
		return nil
	}

	if pubsubMsg.Attributes["assetType"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'assetType' is zero value and should not", "", "")
		return nil
	}

	if pubsubMsg.Attributes["contentType"] == "" {
		glo.LogCriticalNoRetry(ev, "triggering PubSub message attribute 'contentType' is zero value and should not", "", "")
		return nil
	}

	if !strings.Contains(pubsubMsg.Attributes["messageType"], "violation_on_rule") {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("want triggering PubSub message attribute 'messageType' to start with 'violation_on_rule' and got %s", pubsubMsg.Attributes["messageType"]), "", "")
		return nil
	}

	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("%s/%s/%s/%s/%s", pubsubMsg.Attributes["messageType"], pubsubMsg.Attributes["origin"], pubsubMsg.Attributes["assetType"], pubsubMsg.Attributes["contentType"], pubsubMsg.ID),
		StepTimestamp: msgTimestamp,
	}
	ev.StepStack = make(glo.Steps, 0)
	ev.StepStack = append(ev.StepStack, ev.Step)

	finishMsg := ""
	finishDescription := ""

	var violation cai.Violation
	err = json.Unmarshal(pubsubMsg.Data, &violation)
	if err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Unmarshal(b, &violation) %v", err), "", "")
		return nil
	}
	assetType := violation.FeedMessage.Asset.AssetType
	contentType := violation.FeedMessage.ContentType
	ruleName := violation.FunctionConfig.FunctionName
	assetInventoryOrigin := violation.FeedMessage.Origin
	if assetInventoryOrigin != "real-time" {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("Want origin real-time and got %s", assetInventoryOrigin), assetType, ruleName)
		return nil
	}
	if ruleName != global.serviceEnv.RuleName {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("Want ruleName %s and got %s", global.serviceEnv.RuleName, ruleName), assetType, ruleName)
		return nil
	}
	if assetType != global.serviceEnv.AssetType {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("Want assetType %s and got %s", global.serviceEnv.AssetType, assetType), assetType, ruleName)
		return nil
	}

	ev.StepStack = append(violation.StepStack, ev.Step)
	if len(violation.StepStack) == 0 {
		glo.LogCriticalNoRetry(ev, "Want a violation stepstack ang got any", assetType, ruleName)
		return nil
	}
	assetChangeAge := time.Since(ev.StepStack[0].StepTimestamp)
	if assetChangeAge.Minutes() > global.serviceEnv.MaxAssetChangeAgeMinutes {
		finishMsg = "asset change too old did not remediate"
		finishDescription = fmt.Sprintf("want asset change age lower than %v minutes and got %v minutes", global.serviceEnv.MaxAssetChangeAgeMinutes, assetChangeAge.Minutes())
	} else {
		switch global.serviceEnv.ActionKind {
		case "bqdsdelete":
			finishMsg, finishDescription, err = bqdsdelete(ctx, global.bqClient, violation)
			if err != nil {
				if strings.Contains(err.Error(), "dataset.Delete 5xx") {
					glo.LogCriticalRetry(ev, "dataset.Delete 5xx", assetType, ruleName)
					return err
				}
				glo.LogCriticalNoRetry(ev, fmt.Sprintf("bqdsdelete error %s", err.Error()), assetType, ruleName)
				return nil
			}
		default:
			m := fmt.Sprintf("unexpected action kind %s", global.serviceEnv.ActionKind)
			glo.LogCriticalNoRetry(ev, m, "", "")
			return fmt.Errorf(m)
		}
	}
	glo.LogFinish(ev, finishMsg, finishDescription, time.Now(), assetInventoryOrigin, assetType, contentType, ruleName, 0)
	return nil
}
