// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package autofix

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name             string
		messageType      string
		origin           string
		assetType        string
		contentType      string
		missPublishTime  bool
		missOrigin       bool
		missContentType  bool
		envVarActionKind string
		ruleName         string
		path             string
		wantMsgContains  []string
	}{
		{
			name:             "missing_time",
			missPublishTime:  true,
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation01.json",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'timestamp' is missing or invalid and should not",
			},
		},
		{
			name:             "wrong message-type",
			messageType:      "blabla",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation01.json",
			wantMsgContains: []string{
				"noretry",
				"want triggering PubSub message attribute 'messageType' to start with 'violation_on_rule' and got",
			},
		},
		{
			name:             "missing contentType",
			missContentType:  true,
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation01.json",
			wantMsgContains: []string{
				"noretry",
				"triggering PubSub message attribute 'contentType' is zero value and should not",
			},
		},
		{
			name:             "bqdsdelete_finish_too_old",
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation01.json",
			wantMsgContains: []string{
				"finish asset change too old did not remediate",
			},
		},
		{
			name:             "bqdsdelete_invalid_json_violation",
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/fakejson.txt",
			wantMsgContains: []string{
				"CRITICAL",
				"noretry",
				"json.Unmarshal",
				"invalid character",
			},
		},
		{
			name:             "bqdsdelete_not_real-time",
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "blabla",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation02.json",
			wantMsgContains: []string{
				"CRITICAL",
				"noretry",
				"Want origin real-time and got ",
			},
		},
		{
			name:             "bqdsdelete_invalid_ruleName",
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "blabla",
			path:             "testdata/violations/violation03.json",
			wantMsgContains: []string{
				"CRITICAL",
				"noretry",
				"Want ruleName ",
			},
		},
		{
			name:             "bqdsdelete_invalid_assetType",
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "blabla",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation04.json",
			wantMsgContains: []string{
				"CRITICAL",
				"noretry",
				"Want assetType ",
			},
		},
		{
			name:             "bqdsdelete_no_stepStack",
			messageType:      "violation_on_rule.GCPBigQueryDatasetLocationConstraintV1",
			origin:           "real-time",
			assetType:        "bigquery.googleapis.com/Dataset",
			contentType:      "RESOURCE",
			envVarActionKind: "bqdsdelete",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
			path:             "testdata/violations/violation05.json",
			wantMsgContains: []string{
				"CRITICAL",
				"noretry",
				"Want a violation stepstack ang got any",
			},
		},
	}

	ctx := context.Background()
	projectID := os.Getenv("AUTOFIX_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var AUTOFIX_PROJECT_ID")
	}

	now := time.Now()
	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			err := os.Setenv("AUTOFIX_ACTION_KIND", tc.envVarActionKind)
			if err != nil {
				log.Fatalln(err)
			}
			initialize()

			var attr map[string]string
			var pubsubMsg pubsub.Message
			attr = make(map[string]string)

			if !tc.missPublishTime {
				attr["timestamp"] = now.Format(time.RFC3339)
			}
			attr["messageType"] = tc.messageType
			attr["assetType"] = tc.assetType
			attr["contentType"] = tc.contentType
			if tc.missContentType {
				attr["contentType"] = ""
			} else {
				if tc.messageType == "" {
					attr["contentType"] = "RESOURCE"
				} else {
					attr["contentType"] = tc.contentType
				}
			}
			if tc.missOrigin {
				attr["origin"] = ""
			} else {
				if tc.origin == "" {
					attr["origin"] = "real-time"
				} else {
					attr["origin"] = tc.origin
				}
			}
			attr["microserviceName"] = "monitor"
			pubsubMsg.Attributes = attr
			pubsubMsg.ID = "c56c9245-0af7-44c7-8a55-954583940e09"

			p := filepath.Clean(tc.path)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			pubsubMsg.Data = b

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = EntryPoint(ctx, pubsubMsg)
			if err != nil {
				t.Errorf("want no error and got: %v", err)
			}
			msgString := buffer.String()
			// t.Logf("%v", msgString)
			for _, wantMsg := range tc.wantMsgContains {
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
		})
	}
}
