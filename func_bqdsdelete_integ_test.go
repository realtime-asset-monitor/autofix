// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package autofix

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/bigquery"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func TestIntegBqdsdelete(t *testing.T) {
	testCases := []struct {
		name                    string
		fakeViolation           bool
		path                    string
		wantError               string
		createDatasetInLocation string
		addContent              bool
		wantFinishMsg           string
		wantFinishDescription   string
	}{
		{
			name:          "invalid_json_violation",
			fakeViolation: true,
			wantError:     "invalid character",
		},
		{
			name:                    "delete_done",
			path:                    "testdata/violations/violation01.json",
			createDatasetInLocation: "europe-west1",
			wantFinishMsg:           "bqdsdelete done",
		},
		{
			name:                  "not_found",
			path:                  "testdata/violations/violation01.json",
			wantFinishMsg:         "bqdsdelete cancelled",
			wantFinishDescription: "googleapi: Error 404: Not found: Dataset",
		},
		{
			name:                  "unauthorized",
			path:                  "testdata/violations/violation01.json",
			wantFinishMsg:         "bqdsdelete cancelled",
			wantFinishDescription: "googleapi: Error 403: Access Denied: Dataset",
		},
		{
			name:                    "not_empty",
			path:                    "testdata/violations/violation01.json",
			createDatasetInLocation: "europe-west1",
			addContent:              true,
			wantFinishMsg:           "bqdsdelete cancelled",
			wantFinishDescription:   "is still in use",
		},
	}
	ctx := context.Background()
	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var violation cai.Violation
			var datasetName string
			if !tc.fakeViolation {
				p := filepath.Clean(tc.path)
				if !strings.HasPrefix(p, "testdata/") {
					panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
				}
				b, err := os.ReadFile(p)
				if err != nil {
					log.Fatalln(fmt.Errorf("os.ReadFile %v", err))
				}

				err = json.Unmarshal(b, &violation)
				if err != nil {
					log.Fatalln(err)
				}
				type bqDatasetResource struct {
					Data struct {
						DatasetReference struct {
							DatasetID string `json:"datasetId"`
							ProjectID string `json:"projectId"`
						} `json:"datasetReference"`
					} `json:"data"`
				}
				var dsResource bqDatasetResource
				err = json.Unmarshal(violation.FeedMessage.Asset.Resource, &dsResource)
				if err != nil {
					log.Fatalln(err)
				}

				// case create a dataset to be deleted
				if tc.createDatasetInLocation != "" {
					now := time.Now()
					// RFC3339Namo loks like "2006-01-02T15:04:05Z07:00"
					datasetName = strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(fmt.Sprintf("autofix_test_%s_%s", tc.createDatasetInLocation, now.Format(time.RFC3339Nano)), "-", "_"), ":", "_"), ".", "_"), "+", "plus")
					var datasetMetadata bigquery.DatasetMetadata
					datasetMetadata.Location = tc.createDatasetInLocation
					datasetMetadata.Description = "Real-time Asset Monitor autofix test integration"
					dataset := global.bqClient.DatasetInProject(global.serviceEnv.ProjectID, datasetName)
					if err := dataset.Create(ctx, &datasetMetadata); err != nil {
						log.Fatalln(err)
					}
					dsResource.Data.DatasetReference.ProjectID = global.serviceEnv.ProjectID
					dsResource.Data.DatasetReference.DatasetID = datasetName

					// case ds not empty
					if tc.addContent {
						tableName := strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(fmt.Sprintf("autofix_test_%s", time.Now().Format(time.RFC3339Nano)), "-", "_"), ":", "_"), ".", "_"), "+", "plus")
						var tableMetadata bigquery.TableMetadata
						tableMetadata.Description = "Real-time Asset Monitor autofix test integration"
						err = dataset.Table(tableName).Create(ctx, &tableMetadata)
						if err != nil {
							log.Fatalln(err)
						}
					}
				}

				// case unauthrized
				if tc.name == "unauthorized" {
					dsResource.Data.DatasetReference.ProjectID = "brunore-ram-public"
					dsResource.Data.DatasetReference.DatasetID = "ram"
				}

				b, err = json.Marshal(&dsResource)
				if err != nil {
					log.Fatalln(err)
				}
				violation.FeedMessage.Asset.Resource = b
			}
			finishMsg, finishDescription, err := bqdsdelete(ctx, global.bqClient, violation)
			if err != nil {
				if tc.wantError == "" {
					t.Errorf("want no error and got one %s", err.Error())
				} else {
					if strings.Contains(err.Error(), tc.wantError) {
						t.Errorf("Want error %s and got error %s", tc.wantError, err.Error())
					}
				}
			} else {
				if tc.wantError != "" {
					t.Errorf("Want error %s and got none", tc.wantError)
				} else {
					t.Logf("finishMsg %s", finishMsg)
					if tc.wantFinishMsg != "" {
						if !strings.Contains(finishMsg, tc.wantFinishMsg) {
							t.Errorf("want finsh messsage contains %s and got %s", tc.wantFinishMsg, finishMsg)
						}
					}
					// t.Logf("finishDescription %s", finishDescription)
					if tc.wantFinishDescription != "" {
						if !strings.Contains(finishDescription, tc.wantFinishDescription) {
							t.Errorf("want finsh description contains %s and got %s", tc.wantFinishDescription, finishDescription)
						}
					}
				}
			}
			// cleanup test resource
			if datasetName != "" {
				dataset := global.bqClient.DatasetInProject(global.serviceEnv.ProjectID, datasetName)
				err = dataset.DeleteWithContents(ctx)
				if err != nil {
					t.Logf("cleanup warning %v", err)
				}
			}
		})
	}
}
