// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package autofix

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"cloud.google.com/go/bigquery"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func bqdsdelete(ctx context.Context, bqClient *bigquery.Client, violation cai.Violation) (finishMsg string, finishDescription string, err error) {
	b, err := json.MarshalIndent(violation, "", "    ")
	if err != nil {
		return finishMsg, finishDescription, fmt.Errorf("json.MarshalIndent(violation %v", err)
	}
	violationJSON := string(b)

	type bqDatasetResource struct {
		Data struct {
			DatasetReference struct {
				DatasetID string `json:"datasetId"`
				ProjectID string `json:"projectId"`
			} `json:"datasetReference"`
		} `json:"data"`
	}
	var dsResource bqDatasetResource
	err = json.Unmarshal(violation.FeedMessage.Asset.Resource, &dsResource)
	if err != nil {
		return finishMsg, finishDescription, fmt.Errorf("json.Unmarshal(violation.FeedMessage.Asset.Resource %v", err)
	}
	dataset := bqClient.DatasetInProject(dsResource.Data.DatasetReference.ProjectID, dsResource.Data.DatasetReference.DatasetID)
	// Use Delete, DO NOT use Delete With Content, on purpose
	err = dataset.Delete(ctx)
	if err != nil {
		var errSum string
		// cloud.google.com/bigquery/docs/error-messages#:~:text=Response%20codes%20in%20the%205xx,for%20details%20about%20the%20error.
		if strings.Contains(err.Error(), "500") || strings.Contains(err.Error(), "501") || strings.Contains(err.Error(), "503") {
			return finishMsg, finishDescription, fmt.Errorf("dataset.Delete 5xx %v", err)
		}
		switch {
		case strings.Contains(err.Error(), "is still in use"):
			errSum = "as not empty"
		case strings.Contains(err.Error(), "404"):
			errSum = "not found"
		case strings.Contains(err.Error(), "403"):
			errSum = "unauthorized"
		}
		return "bqdsdelete cancelled",
			fmt.Sprintf("%s %s %s %s:%s %v %s",
				err,
				string(violationJSON),
				errSum,
				violation.ConstraintConfig.Kind,
				violation.ConstraintConfig.Metadata.Name,
				dsResource.Data.DatasetReference.ProjectID,
				dsResource.Data.DatasetReference.DatasetID),
			nil
	}
	return "bqdsdelete done",
		fmt.Sprintf("%s %s %s:%s %s",
			violation.ConstraintConfig.Kind,
			violation.ConstraintConfig.Metadata.Name,
			dsResource.Data.DatasetReference.ProjectID,
			dsResource.Data.DatasetReference.DatasetID,
			string(violationJSON)),
		nil
}
