// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package autofix

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"
)

func TestIntegInitialize(t *testing.T) {
	testCases := []struct {
		name             string
		envVarActionKind string
		assetType        string
		ruleName         string
	}{
		{
			name:             "kind_bqdsdelete",
			envVarActionKind: "bqdsdelete",
			assetType:        "bigquery.googleapis.com/Dataset",
			ruleName:         "GCPBigQueryDatasetLocationConstraintV1",
		},
	}

	projectID := os.Getenv("AUTOFIX_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var AUTOFIX_PROJECT_ID")
	}
	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			err := os.Setenv("AUTOFIX_ACTION_KIND", tc.envVarActionKind)
			if err != nil {
				log.Fatalln(err)
			}
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			initialize()
			msgString := buffer.String()
			// t.Log(msgString)
			wantMsgContains := "\"severity\":\"INFO\",\"message\":\"init done\",\"init_id\":"
			if !strings.Contains(msgString, wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", wantMsgContains, msgString)
			}
			if global.serviceEnv.ActionKind != tc.envVarActionKind {
				t.Errorf("want global.serviceEnv.ActionKind to be %s and got %s", tc.envVarActionKind, global.serviceEnv.ActionKind)
			}
			if global.serviceEnv.AssetType != tc.assetType {
				t.Errorf("want global.serviceEnv.AssetType to be %s and got %s", tc.assetType, global.serviceEnv.AssetType)
			}
			if global.serviceEnv.RuleName != tc.ruleName {
				t.Errorf("want global.serviceEnv.RuleName to be %s and got %s", tc.ruleName, global.serviceEnv.RuleName)
			}
		})
	}
}
