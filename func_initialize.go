// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package autofix

import (
	"context"
	"fmt"
	"log"
	"time"

	"cloud.google.com/go/bigquery"
	"cloud.google.com/go/profiler"
	"github.com/google/uuid"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

var global Global
var ctx = context.Background()

// initialize is called by init() and enable tests
func initialize() {
	start := time.Now()
	log.SetFlags(0)
	global.CommonEv.InitID = fmt.Sprintf("%v", uuid.New())
	var err error
	var serviceEnv ServiceEnv

	err = envconfig.Process(microserviceName, &serviceEnv)
	if err != nil {
		log.Println(glo.Entry{
			MicroserviceName: microserviceName,
			Severity:         "CRITICAL",
			Message:          "init_failed",
			Description:      fmt.Sprintf("envconfig.Process %s %v", microserviceName, err),
			InitID:           global.CommonEv.InitID,
		})
		log.Fatalf("INIT_FAILURE %v", err)
	}
	global.serviceEnv = &serviceEnv
	global.CommonEv.MicroserviceName = microserviceName
	global.CommonEv.Environment = global.serviceEnv.Environment
	global.CommonEv.LogOnlySeveritylevels = global.serviceEnv.LogOnlySeveritylevels
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	glo.LogInitColdStart(ev)

	var env Env
	err = envconfig.Process("", &env)
	if err != nil {
		glo.LogInitFatal(ev, "envconfig.Process no prefix", err)
	}
	global.env = &env

	if serviceEnv.StartProfiler {
		err := profiler.Start(profiler.Config{
			ProjectID:            serviceEnv.ProjectID,
			Service:              microserviceName,
			ServiceVersion:       env.KRevision,
			DebugLogging:         false,
			NoGoroutineProfiling: true,
			NoAllocProfiling:     true,
		})
		if err != nil {
			glo.LogInitFatal(ev, "failed to start the profiler", err)
		}
	}

	switch global.serviceEnv.ActionKind {
	case "bqdsdelete":
		if global.serviceEnv.RuleName == "" {
			global.serviceEnv.RuleName = "GCPBigQueryDatasetLocationConstraintV1"
		}
		if global.serviceEnv.AssetType == "" {
			global.serviceEnv.AssetType = "bigquery.googleapis.com/Dataset"
		}
		global.bqClient, err = bigquery.NewClient(ctx, serviceEnv.ProjectID)
		if err != nil {
			glo.LogInitFatal(ev, "bigquery.NewClient(ctx, serviceEnv.ProjectID)", err)
		}
	default:
		err := fmt.Errorf("unexpected action kind %s", global.serviceEnv.ActionKind)
		glo.LogInitFatal(ev, "unexpected action kind", err)
		log.Fatalf("INIT_FAILURE %v", err)
	}
	glo.LogInitDone(ev, time.Since(start).Seconds())
}
